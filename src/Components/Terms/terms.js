import React from 'react';




 const Terms = ({checked,  name, handleChange }) => {
    return(<div>
        <div className="terms">
                        <input type="checkbox"/>
                        <label className="terms_label">I want to receive news and special offers</label>
                    </div>
                    <div className="terms">
                        <input type="checkbox"
                            name={name} 
                            checked={checked}
                            onChange={handleChange}
                            required
                            />
                        <label className="terms_label">I agree with the Terms and Conditions</label>
                    </div>
                    </div>
    )
 };

 export default Terms;