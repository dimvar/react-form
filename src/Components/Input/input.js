import React from 'react';
import './_input.scss';



 const Input = ({icon, length, name, placeholder, value, handleChange, type, formError}) => {
    return(
        <React.Fragment>
        <div className= "form-field">
            {icon}
            <input className={ length  && value  ? "form-field_error" : "form-field_input"}         
                name={name}
                type={type}
                placeholder={placeholder}
                value={value} 
                onChange={handleChange} 
                required
            />
            
        </div>       
       { value  && formError.length > 0 &&(
          <span className="errorMessage">{formError}</span>
          )
       }
    </React.Fragment>
    )
 };

 export default Input;