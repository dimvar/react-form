import React from 'react'

const UserIcon = () => {
    return(
    <i className="material-icons">account_box</i>
    )};
export default UserIcon;