import React from 'react'

const LockOpenIcon = () => {
    return(
    <i className="material-icons">lock_open</i>
    )};
export default LockOpenIcon;