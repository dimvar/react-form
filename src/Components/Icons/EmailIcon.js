import React from 'react'

const EmailIcon = () => {
    return(
    <i className="material-icons">email</i>
    )};
export default EmailIcon;