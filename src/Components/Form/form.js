import React, { Component } from 'react';
import './_form.scss';
import Input from '../Input/input';
import UserIcon from '../Icons/UserIcon';
import EmailIcon from '../Icons/EmailIcon';
import LockOpenIcon from '../Icons/LockOpenIcon';
import DropdownMenu from '../Dropdownmenu/Dropdown';
import Terms from '../Terms/terms';
import SubmitButton from '../SubmitButton/submitButton';


const usernameRegex = RegExp(/^[ a-zA-Z0-9]*$/);
const lastNameRegex = RegExp(/^[ a-zA-Z]*$/);
const passwordRegex = RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/);
const emailRegex = RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
const firstNameRegex = RegExp(/^[a-zA-Z]+$/);

const validators = {
    username: (s) => usernameRegex.test(s),
    email: (s) => emailRegex.test(s),
    password: (s) => passwordRegex.test(s),
    confirmPassword: (s1, s2) => s1 === s2 ,
    firstName: (s) => firstNameRegex.test(s),
    lastName: (s) => lastNameRegex.test(s),
}

const initialState = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    firstName: "",
    lastName: "",
    gender: "",
    agreeWithTerms: "",
    formErrors: {
        username: "",
        email: "",
        password: "",
        confirmPassword: "",
        firstName: "",
        lastName: "",
        gender: "",
        agreeWithTerms: "",
    }
}

const formValid = formErrors => Object.keys(formErrors).every((key) => !formErrors[key]);

export default class Form extends Component {
    
  constructor(props){
    super(props);
    this.state = initialState;
}

handleChange1 = ({ target: { value, name, checked, type } }) => {
    
    const isCheckbox = type === 'checkbox';
    
    this.setState(({ formErrors }) => ({ formErrors: { ...formErrors, [name]: '' } }));
    
    if (isCheckbox) this.setState({ [name]: checked });
    
    this.setState({ [name]: value });

    if (!validators[name](value, this.state.password)) {

        this.setState(({ formErrors }) => ({ formErrors: { ...formErrors, [name]: `Invalid ${name}` } }));
    }
}

handleSubmit = e => {
    e.preventDefault();
   
    if (formValid(this.state.formErrors)) {
        console.log(`
            --SUBMITTING--
            User Name: ${this.state.username}
            Email: ${this.state.email}
            Password: ${this.state.password}
            Confirm Password: ${this.state.confirmPassword}
            First Name: ${this.state.firstName}
            Last Name: ${this.state.lastName}
            Gender: ${this.state.gender}
        `)
    } else {
        console.error(`FORM INVALID`)
    }

    this.setState(initialState);
}
  
  render() {
        return (
          <form onSubmit={this.handleSubmit} className="form-wrapper"> 
            <h2>Registration form</h2>
                    <Input 
                        icon={<UserIcon/>}
                        length= {this.state.formErrors.username.length}
                        type="text"
                        placeholder="Username"
                        name="username"
                        value={this.state.username}
                        handleChange={this.handleChange1}
                        formError={this.state.formErrors.username}
                    />                    
                    <Input 
                        icon={<EmailIcon/>}
                        type="email"
                        placeholder="Email"
                        name="email"
                        value={this.state.email}
                        handleChange={this.handleChange1}
                        length = {this.state.formErrors.email.length}
                        formError={this.state.formErrors.email}
                    />
                   <div className='form-field'>
                   <Input 
                        icon={<LockOpenIcon/>}
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={this.state.password}
                        handleChange={this.handleChange1}
                        formError={this.state.formErrors.password}
                    />
                   <Input 
                        type="password"
                        placeholder="Confirm Password"
                        name="confirmPassword"
                        value={this.state.confirmPassword}
                        handleChange={this.handleChange1}
                        formError={this.state.formErrors.confirmPassword}
                    />
                   </div>
                     <h3>Personal details</h3>
                     <div className='form-field'>
                   <Input 
                        type="text"
                        placeholder="First Name"
                        name="firstName"
                        value={this.state.firstName}
                        handleChange={this.handleChange1}
                        formError={this.state.formErrors.firstName}
                    />
                   <Input 
                        type="text"
                        placeholder="Last Name"
                        name="lastName"
                        value={this.state.lastName}
                        handleChange={this.handleChange1}
                        formError={this.state.formErrors.lastName}
                    />
                   </div>
                    <DropdownMenu 
                        name="gender"
                        value={this.state.gender}
                        handleChange={this.handleChange1}
                    />
                    <Terms 
                        name="agreeWithTerms"
                        checked={this.state.agreeWithTerms}
                        handleChange={this.handleChange1}
                    />
                    <SubmitButton />
          </form>
            );
      }
}