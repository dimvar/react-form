import React from "react";
import './_dropdown.scss';


const DropdownMenu = ({value, name, handleChange}) => {
  return(
    <label className="dropdown_title">Gender
  <select className="dropdown" name={name} value={value}  onChange={handleChange} required >
      <option className="gender-placeholder" hidden > </option>
      <option>Male</option>
      <option>Female</option>
      <option>Other</option>

      </select>
      </label>
  )
};

export default DropdownMenu;
